@extends('product.layout')

@section('content')
<ul>
    @foreach ($products as $product)
        <li>{{{$product->name}}}</li>
        <li>{{{$product->price}}}</li>
    @endforeach
</ul>
@stop