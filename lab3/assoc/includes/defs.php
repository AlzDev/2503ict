<?php
/* Functions for PM database example. */

/* Load sample data, an array of associative arrays. */
include "pms.php";

/* Search sample data for $name or $year or $state from form. */
function search($find) {
    global $pms; 

    // Filter $pms by $find
    if (!empty($find)) {
	$results = array();
	foreach ($pms as $pm) {
	    if (stripos($pm['name'], $find) !== FALSE || strpos($pm['from'], $find) !== FALSE || strpos($pm['to'], $find) !== FALSE || stripos($pm['state'], $find) !== FALSE) {
		$results[] = $pm;
	    }
	}
	$pms = $results;
    }
    return $pms;
}
?>
