<?php


class ProductSeeder extends Seeder{
    public function run()
    {
        // Remove the table to seed from a clean slate.
        DB::table('products')->delete();
        
        $product = new Product;
        $product->name = 'Peanuts';
        $product->price = '99';
        $product->save();
        
        $product = new Product;
        $product->name = 'PURPLE LLAMA';
        $product->price = '100000000000';
        $product->save();
    }
}
?>