<?php
    $posts = array(
            array('date'=>'10/12/16','message' => ' I like turtles 1', 'image'=> 'images/image.jpg'),
            array('date'=>'11/12/16','message' => ' I like turtles 2', 'image'=> 'images/image.jpg'),
            array('date'=>'12/12/16','message' => ' I like turtles 3', 'image'=> 'images/image.jpg'),
            array('date'=>'13/12/16','message' => ' I like turtles 4', 'image'=> 'images/image.jpg'),
            array('date'=>'14/12/16','message' => ' I like turtles 5', 'image'=> 'images/image.jpg'),
            array('date'=>'15/12/16','message' => ' I like turtles 6', 'image'=> 'images/image.jpg'),
            array('date'=>'16/12/16','message' => ' I like turtles 7', 'image'=> 'images/image.jpg'),
            array('date'=>'17/12/16','message' => ' I like turtles 8', 'image'=> 'images/image.jpg'),
            array('date'=>'18/12/16','message' => ' I like turtles 9', 'image'=> 'images/image.jpg'),
            array('date'=>'19/12/16','message' => ' I like turtles 10', 'image'=> 'images/image.jpg')
        );
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Social Network</title>

    <!-- Bootstrap core CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
  </head>

  <body>

    <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Socail Network</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            
            <ul class="nav navbar-nav navbar-right">
              <li><a href="./">Photos</a></li>
              <li><a href="../navbar-static-top/">Friends</a></li>
              <li><a href="../navbar-fixed-top/">LogIn</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

      <!-- Main body -->
      <div class='row'>
          <div class='col-sm-2 alert alert-success' role='alert' id='left'>
              <form>
                  Name: <br>
                  <input class ='well' id='nametext'type='text' name='name'><br>
                  Message: <br>
                  <textarea class='messagebox' rows='4' cols='10'>Enter your post here!</textarea>
                  <br>
                  <button class='btn btn-info'>Post!</button>
              </form>
          </div>
          <div class='col-sm-10' id='right'>
              <h1 class='nameHead'>Alan Schiffler</h1>
              <?php
                $num = rand(1,10);
                echo "<br> $num <br>";
             
                $i = 0;
                foreach($posts as $post) {
                  
                  if ($i >= $num) {
                    break;
                  }
                                   /*Original Loop Proper
                                           
+               $num = rand(1,10);
+               echo "<br> $num <br>";
+             
+                for ($i=0; $i < $num; $i++){
+                $image = $posts[$i]['image'];
+                $message = $posts[$i]['message'];
+                $date = $posts[$i]['date'];
+                echo "<div class='post bg-info'>";
+                echo "<img class='photo' src='$image' alt='Galexy Photo'>" ;
+                echo $date ;
+                echo $message;
+                echo "</div>";
+               }
+     */
                  $image = $post['image'];
                  $message = $post['message'];
                  $date = $post['date'];
                  echo "<div class='post bg-info'>";
                  echo "<img class='photo' src='$image' alt='Galexy Photo'>" ;
                  echo $date ;
                  echo $message;
                  echo "</div>";
                  
                  $i++;
                }
             
            ?>

          </div>
      </div>

    </div> <!-- /container -->



  </body>
</html>